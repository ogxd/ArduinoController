﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;

namespace ArduinoSharp {
    public class ArduinoController {

        #region STATIC
        /// <summary>
        /// Returns a ArduinoController if the connection was successful. Specify a COM port if you want to target a specific board.
        /// Leave the field empty or null to connect automatically to the first board detected.
        /// </summary>
        /// <param name="port"></param>
        public static ArduinoController GetController(string port = null, bool logEnabled = false) {

            if (string.IsNullOrEmpty(port)) {
                if (logEnabled) {
                    Console.WriteLine("> Looking for a connected Arduino board...");
                    Console.WriteLine("> Available ports : ");
                }
                string[] _ports = SerialPort.GetPortNames();
                if (_ports.Length == 0) {
                    if (logEnabled)
                        Console.WriteLine("> No arduino was detected");
                    return null;
                }
                if (logEnabled) {
                    foreach (string _port in _ports)
                        Console.WriteLine("    > " + _port);
                }
                return new ArduinoController(_ports.First(), logEnabled);
            } else {
                return new ArduinoController(port, logEnabled);
            }
        }
        #endregion

        #region INSTANCE

        #region Public vars and props
        /// <summary>
        /// Returns the serial port.
        /// </summary>
        public SerialPort serial { get; private set; }

        /// <summary>
        /// Button events delegate.
        /// </summary>
        /// <param name="digitalPin"></param>
        public delegate void ButtonEventHandler(int digitalPin);

        /// <summary>
        /// This event is triggered when a button is pressed. The digitalPin argument indicates the pin on which this button is.
        /// </summary>
        public event ButtonEventHandler ButtonPressed;

        /// <summary>
        /// This event is triggered when a button is released. The digitalPin argument indicates the pin on which this button is.
        /// </summary>
        public event ButtonEventHandler ButtonReleased;
        #endregion


        #region Private vars and props
        private bool _logEnabled = false;
        private Dictionary<int, bool> _buttonsStatus = new Dictionary<int, bool>();
        #endregion


        #region Public methods
        public ArduinoController(string _port, bool _logEnabled) {

            this._logEnabled = _logEnabled;

            serial = new SerialPort(_port, 9600, Parity.None, 8, StopBits.One);
            serial.Open();
            Console.WriteLine("> Context opened on port " + _port);

            Thread myThread;
            myThread = new Thread(new ThreadStart(_receive));
            myThread.Start();
        }

        /// <summary>
        /// Returns True or False weither the button on the specified pin is pressed or not.
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        public bool IsButtonPressed(int pin) {
            if (!_buttonsStatus.ContainsKey(pin)) {
                return false;
            } else {
                return _buttonsStatus[pin];
            }
        }
        #endregion


        #region Private methods
        /// <summary>
        /// Receiving loop (threaded);
        /// </summary>
        private void _receive() {
            int pin;
            while (true) {
                pin = serial.ReadByte();
                if (pin < 100) {
                    _setButtonStatus(pin, true);
                    ButtonPressed?.Invoke(pin);
                    _log("Button " + pin + " pressed !");
                } else {
                    pin -= 100;
                    _setButtonStatus(pin, false);
                    ButtonReleased?.Invoke(pin);
                    _log("Button " + pin + " released !");
                }
            }
        }

        /// <summary>
        /// Set the button status for Button Status follow
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="status"></param>
        private void _setButtonStatus(int pin, bool status) {
            if (!_buttonsStatus.ContainsKey(pin)) {
                _buttonsStatus.Add(pin, status);
            } else {
                _buttonsStatus[pin] = status;
            }
        }

        /// <summary>
        /// Log in console
        /// </summary>
        /// <param name="_message"></param>
        private void _log(string _message) {
            if (_logEnabled) {
                Console.WriteLine("> " + _message);
            }
        }
        #endregion

        #endregion
    }
}
