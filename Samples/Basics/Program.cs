﻿using System;

namespace ArduinoSharp {

    class Program {

        static void Main(string[] args) {

            ArduinoController arduino = ArduinoController.GetController();

            arduino.ButtonPressed += Arduino_ButtonPressed;

            Console.ReadKey();
        }

        private static void Arduino_ButtonPressed(int digitalPin) {

        }
    }
}
