# ArduinoController

ArduinoController is a lightweight library written in C# to give .NET applications receiving digital inputs from Arduino boards.

## Quick guide
- Download or clone the repository
- Open ArduinoController.ino on the Arduino IDE and upload the code yo your board (tested on Arduino UNO)
- Connect a button on a digital pin of your Arduino board (with resistor, I personally use 10kohm)
- Plug in Arduino with USB
- Compile ArduinoController and run the sample (Basics). Press the button on your arduino and check the result :)

## How to wire a button ?
Follow the standard button montage (Arduino official website)
![Arduino Button Scheme](https://github.com/Ogx8/ArduinoController/tree/master/Images/button_schem.png)

## What can I do with it ?
Here are the current possibilities :
- Receive events when a button is pressed / released.
- Check at anytime the status of a button.

For now, the library handles one button per digital pin. This could be easily improved by sharing pins and sending different signatures on the serial, but a the cost of the ease of use, which is currently very easy.
Only button are supported and the communication is one-way. This may change in the future if my personnal projects require me to do so.